#!/usr/bin/bash
# Prepare the host environment for running the osbuild unit tests.
# This includes installing missing dependencies and tools.

set -euxo pipefail

source /etc/os-release

case "${ID}" in
    fedora)
        PKG_MAINT_TOOL="fedpkg"
        sudo dnf install -y "${PKG_MAINT_TOOL}"
        ;;
    rhel)
        PKG_MAINT_TOOL="rhpkg"
        # rhpkg is not available in the default repos
        sudo curl -L -o "/etc/yum.repos.d/rcm-tools-rhel-${VERSION_ID%.*}-baseos.repo" "http://download.devel.redhat.com/rel-eng/internal/rcm-tools-rhel-${VERSION_ID%.*}-baseos.repo"
        # install the RH IT CA certificate used by the repo above
        sudo curl -L -o /etc/pki/ca-trust/source/anchors/2015-IT-Root-CA.pem http://certs.corp.redhat.com/certs/2015-IT-Root-CA.pem
        sudo curl -L -o /etc/pki/ca-trust/source/anchors/2022-IT-Root-CA.pem http://certs.corp.redhat.com/certs/2022-IT-Root-CA.pem
        sudo update-ca-trust
        ;;
    centos)
        PKG_MAINT_TOOL="centpkg"
        # centpkg is not available in the default repos
        sudo dnf install -y epel-release
        # CRB repos are available only for CentOS Stream 9
        if [ "${VERSION_ID}" == "9" ]; then
            dnf config-manager --set-enabled crb
        fi
        ;;
    *)
        echo "Error: unsupported OS: ${ID}-${VERSION_ID}"
        exit 1
        ;;
esac

# Move to the checked out git repo with the test plans
# this should be the root of the dist-git repo
cd "${TMT_TREE}"

# install all test dependencies
sudo dnf install -y \
    "${PKG_MAINT_TOOL}" \
    rpmdevtools \
    python3-mako \
    python3-pip \
    rpm-ostree \
    dosfstools \
    gdisk
sudo dnf builddep -y osbuild.spec

# Install pytst from pip, because the version in some RHEL / CentOS releases is too old
sudo pip3 install pytest

# Make sure that /usr/lib/systemd/boot/efi/linuxx64.efi.stub is available to enable pe32p tests
case "${ID}-${VERSION_ID}" in
    rhel-8.* | centos-8)
        sudo dnf install -y systemd-udev
        ;;
    *)
        sudo dnf install -y systemd-boot-unsigned
        ;;
esac
